#include "serial.h"
#include "ui_serial.h"

Serial::Serial(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Serial)
{
    ui->setupUi(this);
    this->setFixedSize(660, 480);

    /*
     *  用户实现
     */
    User();
}

Serial::~Serial()
{
    delete ui;
}


/*
 *  用户实现
 */
void Serial::User(void)
{
    if(ConfigInit)
    {
        FindCOM();      /*搜索系统串口*/

        /*设置串口配置默认参数*/
        ui->comboBoxCOM->setCurrentIndex(0);
        port->setPortName(ui->comboBoxCOM->currentText());
        ui->comboBoxBaud->setCurrentIndex(7);
        port->setBaudRate(QSerialPort::Baud115200);
        ui->comboBoxDataBit->setCurrentIndex(3);
        port->setDataBits(QSerialPort::Data8);
        ui->comboBoxCheckBit->setCurrentIndex(0);
        port->setParity(QSerialPort::NoParity);
        ui->comboBoxStopBit->setCurrentIndex(0);
        port->setStopBits(QSerialPort::OneStop);
        /*禁止操作*/
        ui->pushButtonDataTX->setEnabled(false);
        ui->pushButtonClearTX->setEnabled(false);
        ui->pushButtonClearRX->setEnabled(false);
        ui->checkBoxHexRX->setEnabled(false);
        ui->checkBoxHexTX->setEnabled(false);
        ui->checkBoxAutoClearTX->setEnabled(false);
        ui->lineEditDataTX->setEnabled(false);
        ui->textBrowserDataRX->setEnabled(false);
        ui->textBrowserDataRX->setReadOnly(true);

        /*默认设置自动滚动*/
        ui->checkBoxAutoRoll->setCheckState(Qt::Checked);
        ui->checkBoxAutoRoll->setEnabled(false);

        ConfigInit = false;
    }
    /*串口开关控制*/
    connect(ui->pushButtonSerialSwitch, &QPushButton::clicked, [=](){
        Serial::PortSwitch();
    });

    /*端口刷新*/
    connect(ui->pushButtonUpData, &QPushButton::clicked, [=](){
        Serial::PortUpdata();
    });

    /*端口号设置*/
    connect(ui->comboBoxCOM, &QComboBox::currentTextChanged, [=](){
        Serial::SetPort();
    });

    /*波特率设置*/
    connect(ui->comboBoxBaud, pBaudRate, [=](int index){
        Serial::SetBaudRate(index);
    });

    /*数据位设置*/
    connect(ui->comboBoxDataBit, pDataBit, [=](int index){
        Serial::SetDataBit(index);
    });

    /*校验位设置*/
    connect(ui->comboBoxCheckBit, pCheckBit, [=](int index){
        Serial::SetCheckBit(index);
    });

    /*停止位设置*/
    connect(ui->comboBoxStopBit, pStopBit, [=](int index){
        Serial::SetStopBit(index);
    });

    /*发送数据*/
    connect(ui->pushButtonDataTX, &QPushButton::clicked, [=](){
        Serial::SendData();
    });

    /*接收数据*/
    connect(port, &QSerialPort::readyRead, [=](){
        Serial::ReceiveData();
    });

    /*清空接收区*/
    connect(ui->pushButtonClearRX, &QPushButton::clicked, [=](){
       Serial::ClearDataRX();
    });

    /*清空发送区*/
    connect(ui->pushButtonClearTX, &QPushButton::clicked, [=](){
       Serial::ClearDataTX();
    });

    /*内容自动滚动*/
    connect(ui->textBrowserDataRX, &QTextBrowser::textChanged, [=](){
        if(ui->checkBoxAutoRoll->checkState() == Qt::Checked)
        {
            Serial::AutoRoll();
        }
    });
}

/*
 *  槽函数
 */

/*串口开关控制*/
void Serial::PortSwitch(void)
{
    if(!PortStatus)
    {
        Serial::OpenPort();
        ui->pushButtonSerialSwitch->setText("关闭串口");
        PortStatus = true;
    }
    else
    {
        Serial::ClosePort();
        ui->pushButtonSerialSwitch->setText("打开串口");
        PortStatus = false;
    }
}

/*打开串口*/
void Serial::OpenPort(void)
{
    if(PortStatus == false)
    {
        port->open(QIODevice::ReadWrite);
        /*禁止串口配置参数操作*/
        ui->comboBoxCOM->setEnabled(false);
        ui->comboBoxBaud->setEnabled(false);
        ui->comboBoxDataBit->setEnabled(false);
        ui->comboBoxCheckBit->setEnabled(false);
        ui->comboBoxStopBit->setEnabled(false);
        ui->pushButtonUpData->setEnabled(false);
        ui->pushButtonDataTX->setEnabled(true);
        ui->pushButtonClearTX->setEnabled(true);
        ui->pushButtonClearRX->setEnabled(true);
        ui->checkBoxHexRX->setEnabled(true);
        ui->checkBoxHexTX->setEnabled(true);
        ui->checkBoxAutoClearTX->setEnabled(true);
        ui->lineEditDataTX->setEnabled(true);
        ui->textBrowserDataRX->setEnabled(true);
        ui->checkBoxAutoRoll->setEnabled(true);
    }
}

/*关闭串口*/
void Serial::ClosePort(void)
{
    if(PortStatus == true)
    {
        port->close();
        /*使能串口配置参数操作*/
        ui->comboBoxCOM->setEnabled(true);
        ui->comboBoxBaud->setEnabled(true);
        ui->comboBoxDataBit->setEnabled(true);
        ui->comboBoxCheckBit->setEnabled(true);
        ui->comboBoxStopBit->setEnabled(true);
        ui->pushButtonUpData->setEnabled(true);
        ui->pushButtonDataTX->setEnabled(false);
        ui->pushButtonClearTX->setEnabled(false);
        ui->pushButtonClearRX->setEnabled(false);
        ui->checkBoxHexRX->setEnabled(false);
        ui->checkBoxHexTX->setEnabled(false);
        ui->checkBoxAutoClearTX->setEnabled(false);
        ui->lineEditDataTX->setEnabled(false);
        ui->textBrowserDataRX->setEnabled(false);
        ui->checkBoxAutoRoll->setEnabled(false);
    }

    Serial::ClearDataRX();
    Serial::ClearDataTX();
}

/*端口刷新*/
void Serial::PortUpdata(void)
{
    ui->comboBoxCOM->clear();
    Serial::FindCOM();
    Serial::SetPort();
}

/*端口号设置*/
void Serial::SetPort()
{
    port->setPortName(ui->comboBoxCOM->currentText());
}

/*端口搜索*/
void Serial::FindCOM(void)
{
    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
         port->setPort(info);
         if(port->open(QIODevice::ReadWrite))
         {
             ui->comboBoxCOM->addItem(info.portName());
             port->close();
         }
    }
}

/*波特率设置*/
void Serial::SetBaudRate(int index)
{
    switch (index) {
        case 0:
            port->setBaudRate(QSerialPort::Baud1200);
            break;
        case 1:
            port->setBaudRate(QSerialPort::Baud2400);
            break;
        case 2:
            port->setBaudRate(QSerialPort::Baud4800);
            break;
        case 3:
            port->setBaudRate(QSerialPort::Baud9600);
            break;
        case 4:
            port->setBaudRate(QSerialPort::Baud19200);
            break;
        case 5:
            port->setBaudRate(QSerialPort::Baud38400);
            break;
        case 6:
            port->setBaudRate(QSerialPort::Baud57600);
            break;
        case 7:
            port->setBaudRate(QSerialPort::Baud115200);
            break;
        default:
            break;
    }
}

/*数据位设置*/
void Serial::SetDataBit(int index)
{
    switch (index) {
        case 0:
            port->setDataBits(QSerialPort::Data5);
            break;
        case 1:
            port->setDataBits(QSerialPort::Data6);
            break;
        case 2:
            port->setDataBits(QSerialPort::Data7);
            break;
        case 3:
            port->setDataBits(QSerialPort::Data8);
            break;
        default:
            break;
    }
}

/*校验位设置*/
void Serial::SetCheckBit(int index)
{
    switch (index) {
        case 0:
            port->setParity(QSerialPort::NoParity);
            break;
        case 1:
            port->setParity(QSerialPort::OddParity);
            break;
        case 2:
            port->setParity(QSerialPort::EvenParity);
            break;
        default:
            break;
    }
}

/*停止位设置*/
void Serial::SetStopBit(int index)
{
    switch (index) {
        case 0:
            port->setStopBits(QSerialPort::OneStop);
            break;
        case 1:
            port->setStopBits(QSerialPort::TwoStop);
            break;
        default:
            break;
    }
}

/*发送数据*/
void Serial::SendData(void)
{
    QString data = ui->lineEditDataTX->text();
    QByteArray bytearray = data.toLatin1();

    if(ui->checkBoxHexTX->checkState() == Qt::Checked)
    {
        port->write(bytearray.toHex(' ').append(' '));
    }
    else
    {
        port->write(bytearray);
    }

    if(ui->checkBoxAutoClearTX->checkState() == Qt::Checked)
    {
        ui->lineEditDataTX->clear();
        RXDataSize = 0;
    }
    else
    {
        TXDataSize += bytearray.size();
    }

    if(TXDataSize >= 99999)
    {
        TXDataSize = 0;
        Serial::ClearDataTX();
    }

    Serial::ShowDataRT();
}

/*接收数据*/
void Serial::ReceiveData(void)
{
    QByteArray bytearray = port->readAll();
    if(ui->checkBoxHexRX->checkState() == Qt::Checked)
    {
        ui->textBrowserDataRX->insertPlainText(QString(bytearray.toHex(' ').append(' ').toUpper()));
    }
    else
    {
        ui->textBrowserDataRX->insertPlainText(QString::fromLocal8Bit(bytearray));
        qDebug() << QString(bytearray) << endl;
    }

    RXDataSize += bytearray.size();
    if(RXDataSize >= 99999)
    {
        RXDataSize = 0;
        Serial::ClearDataRX();
    }
    Serial::ShowDataRT();
}

/*清空接收区*/
void Serial::ClearDataRX(void)
{
    ui->textBrowserDataRX->clear();
    RXDataSize = 0;
    Serial::ShowDataRT();
}

/*清空发送区*/
void Serial::ClearDataTX(void)
{
    ui->lineEditDataTX->clear();
    TXDataSize = 0;
    Serial::ShowDataRT();
}

/*显示接收和发送的数据大小*/
void Serial::ShowDataRT(void)
{
    ui->lcdNumberDataByteRX->display(RXDataSize);
    ui->lcdNumberDataByteTX->display(TXDataSize);
}

/*接收区内容满屏自动滚动*/
void Serial::AutoRoll(void)
{
    ui->textBrowserDataRX->moveCursor(QTextCursor::End);
}
