#ifndef SERIAL_H
#define SERIAL_H

#include <QMainWindow>
#include <QSerialPort>
#include <QComboBox>
#include <QTextBrowser>
#include <QSerialPortInfo>
#include <QTextCursor>
#include <QDebug>


QT_BEGIN_NAMESPACE
namespace Ui { class Serial; }
QT_END_NAMESPACE

class Serial : public QMainWindow
{
    Q_OBJECT

public:
    Serial(QWidget *parent = nullptr);
    ~Serial();

private slots:
    void PortSwitch(void);          /*串口开关控制*/
    void OpenPort(void);            /*打开串口*/
    void ClosePort(void);           /*关闭串口*/
    void PortUpdata(void);          /*端口刷新*/
    void SetPort();                 /*设置端口*/
    void FindCOM(void);             /*端口初搜索*/
    void SetBaudRate(int index);    /*设置波特率*/
    void SetDataBit(int index);     /*设置数据位*/
    void SetCheckBit(int index);    /*设置校验位*/
    void SetStopBit(int index);     /*设置停止位*/
    void SendData(void);            /*发送数据*/
    void ReceiveData(void);         /*接收数据*/
    void AutoRoll(void);            /*接收区内容满屏自动滚动*/
    void ClearDataRX(void);         /*清空接收区*/
    void ClearDataTX(void);         /*清空发送区*/
    void ShowDataRT(void);          /*显示接收和发送的数据大小*/

private:
    Ui::Serial *ui;

    /*
     *  用户实现
     */
    bool PortStatus = false;        /*串口状态*/
    bool ConfigInit = true;         /*默认参数设置*/

    void User(void);                /*串口初始化*/

    QSerialPort *port = new QSerialPort();      /*定义一个端口*/

    int RXDataSize = 0;             /*计算接收数据的大小*/
    int TXDataSize = 0;             /*计算发送数据的大小*/

    /*波特率*/
    void (QComboBox::*pBaudRate)(int) = &QComboBox::currentIndexChanged;
    /*数据位*/
    void (QComboBox::*pDataBit)(int) = &QComboBox::currentIndexChanged;
    /*校验位*/
    void (QComboBox::*pCheckBit)(int) = &QComboBox::currentIndexChanged;
    /*停止位*/
    void (QComboBox::*pStopBit)(int) = &QComboBox::currentIndexChanged;
};
#endif // SERIAL_H
